#!/usr/bin/ruby
#-*- coding: utf-8 -*-

require 'sinatra'
require 'open3'

STORAGE_IMAGE = '/opt/rpi-ums2web/usb.img'
SUPPORT_TYPES = {
  '.jpg' => [:jpg, 'image/jpeg'],
  '.png' => [:png, 'image/png']
}

configure do
  set :bind, '0.0.0.0'
  set :port, 8080
  SUPPORT_TYPES.each_value do |sym, mime|
    mime_type sym, mime
  end
end

get '/' do
  o, e, s = Open3.capture3('mdir', '-i', STORAGE_IMAGE, '-b', '-s')
  files = o.split("\n").delete_if{|f|
    not SUPPORT_TYPES.keys.include?(File.extname(f).downcase)
  }.collect{|f|
    f.tr(':', '')
  }
  erb(:viewer, locals: {files: files})
end

get '/images/*' do |img_path|
  unless type = SUPPORT_TYPES[File.extname(img_path).downcase]
    raise Sinatra::NotFound
  end
  content_type(type[0])
  o, e, s = Open3.capture3('mtype', '-i', STORAGE_IMAGE, "::/#{img_path}")
  raise Sinatra::NotFound, e + "\n" + s.inspect unless s.success?
  o
end
