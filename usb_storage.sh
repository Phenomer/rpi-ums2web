#!/bin/sh

set -e
set -x

STORAGE_IMAGE='/opt/rpi-ums2web/usb.img'
STORAGE_SIZE=128

modprobe libcomposite
modprobe usb_f_mass_storage

mkdir -p /sys/kernel/config/usb_gadget/pistorage
cd /sys/kernel/config/usb_gadget/pistorage

echo 0x1d6b > idVendor
echo 0x0104 > idProduct
echo 0x0100 > bcdDevice
echo 0x0200 > bcdUSB

mkdir -p strings/0x409
echo "39393939"           > strings/0x409/serialnumber
echo "NegiPi"             > strings/0x409/manufacturer
echo "NegiPi USB Storage" > strings/0x409/product

mkdir -p configs/pistorage.1/strings/0x409
echo 120 > configs/pistorage.1/MaxPower

mkdir -p functions/mass_storage.usb0
ln -sf functions/mass_storage.usb0 configs/pistorage.1/mass_storage.usb0 || true

if [ ! -e "${STORAGE_IMAGE}" ]; then
  dd if=/dev/zero of="${STORAGE_IMAGE}" bs=1M count=${STORAGE_SIZE}
  mformat -v NegiPi -t ${STORAGE_SIZE}M -h 64 -s 32 -i "${STORAGE_IMAGE}"
fi
echo "${STORAGE_IMAGE}" > functions/mass_storage.usb0/lun.0/file

echo '' > UDC || true
ls /sys/class/udc > UDC
