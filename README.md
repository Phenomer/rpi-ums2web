# これはなに?
Raspberry Pi ZeroなどをUSB mass storageに見せかけ、
記録された画像ファイルをWeb経由で参照できるようにするやつです。


# 準備
## USB-OTGの有効化
### /boot/config.txt
```
dtoverlay=dwc2
```

## 依存ライブラリのインストール
### Raspbianの場合
```sh
# apt install mtools ruby2.5 ruby-bundler
```

### Arch Linuxの場合
```sh
# pacman -S mtools ruby ruby-bundler
```

## Webアプリ用ライブラリのインストール
```sh
# cd /opt
# git clone https://bitbucket.org/Phenomer/rpi-ums2web
# cd rpi-ums2web
# bundle config set path vendor/bundle
# bundle install
```

# つかいかた
## USB mass storage機能の有効化
```sh
# ./usb_storage.sh
```

## Webインタフェースの起動
```sh
# bundle exec ./app.rb
```
http://raspberry piのIPアドレスまたはホスト名:8080/ にアクセス!


# 起動時に自動実行
起動時にUSB mass storage機能の有効化とWebインタフェースの起動を自動でおこないたい場合は、
下記のようにsystemdに登録します。
```sh
# cd /opt/rpi-ums2web
# cp rpi-ums2web.service /etc/systemd/system
# systemctl enable --now rpi-ums2web.service
```


# ディスクイメージの移動・サイズ変更
デフォルトでは/opt/rpi-ums2web/usb.imgに128MBのファイルシステムイメージファイルが作成されます。
場所を変更する場合は、usb_storage.shとapp.rbのSTORAGE_IMAGE変数を、
サイズを変更する場合は、usb_storage.shのSTORAGE_SIZE変数を編集してください。


# TODO
- 権限まわりの整理
- インタフェースの改善
  - ページャを実装する
  - mastodon投稿とか

# ライセンス
MIT Licenseです。
